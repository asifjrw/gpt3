import React from 'react';
import { Atlasian, DropBox, Google, Shopify, Slack } from '../import';
import './brand.css'

const Brand = () => {
  return (
    <div className='gpt3__brand section__padding'>
      <div>
        <img src={Google} alt="google" />
      </div>
      <div>
        <img src={Slack} alt="slack" />
      </div>
      <div>
        <img src={Atlasian} alt="atlasian" />
      </div>
      <div>
        <img src={DropBox} alt="dropbox" />
      </div>
      <div>
        <img src={Shopify} alt="shopify" />
      </div>
    </div>
  )
}

export default Brand