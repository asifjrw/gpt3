import Google from '../assets/google.png';
import Slack from '../assets/slack.png';
import Atlasian from '../assets/atlassian.png';
import DropBox from '../assets/dropbox.png';
import Shopify from '../assets/shopify.png';

export {
    Google,
    Slack,
    Atlasian,
    DropBox,
    Shopify
}